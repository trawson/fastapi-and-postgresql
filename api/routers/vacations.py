##Any new routes goes in routers
from fastapi import APIRouter
from queries.vacations import VacationIn


router = APIRouter()

#FastAPI is awesome, it takes all the information and turns it into an object
#automatically for us - no need for JSON.loads!
@router.post("/vacations")
#We make vacation of TYPE VacationIn (Think TypeScript)
def create_vacation(vacation: VacationIn):
    #print('vacation', vacation.name)
    return vacation
