##Anything to access the database is in queries
from pydantic import BaseModel
from typing import Optional

#NOTHING TO DO WITH THE DATABASE, dont be confused by BaseModel
#BaseModels are there for us to look at and understand the shape of our
#ENDPOINTS
#It takes our JSON and converts it into this format (an object)
##This is information that is coming IN to our program
class VacationIn(BaseModel):
    #Notice the types - TypeScript brain
    name: str
    from_date: str
    to_date: str
    thoughts: Optional[str]
